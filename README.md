# What is this?

This is a project that was developed in 2008 to collect how many "things" were passed by the sensor and send by Infra-Red to a Palm Top (too old right...)

# What do you need?

you need to install:
 - git
 - CCS PIC C Compiler
 - Eclipse (Optional, the eclipse environment is available, but it is not mandatory).
 - The project was done in Windows
 - The Hardware (Schematics is not available yet)

# How do I build?

You need open a project in CCS IDE and add all files in it. Once you have create the project you can just compile it and burn the chip.

# The project for Palm-Top to read this information is not available

Sorry, I don't have all the files anymore











/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file config.c
 *
 * @author Thiago Esteves
 * @date 07/2008
 */
   
//******************** Includes para o arquivo *****************************//

#include "config.h"

//******************* Funções deste Módulo *********************************//

/**
 *  Faz as configurações iniciais do PIC do equipamento
 *
 */
void init_device(void)
{
	// Configurações de portas ----------------------------------------------
	// 0-> Output 1-> Input
	port_b_pullups(TRUE);
	
	// Interrupções Globais -------------------------------------------------
	enable_interrupts ( GLOBAL );
	
	// Interrupção de Recepção de RS232 -------------------------------------
	enable_interrupts ( INT_RDA );
	
	// Interrupção de Envio de RS232 ----------------------------------------
	disable_interrupts ( INT_TBE );
	
	// Configura pora A0 para leitura da porta Analógica --------------------
	setup_adc_ports(AN0);
	setup_adc(ADC_CLOCK_INTERNAL);
	set_adc_channel(0);
}

//***************************************************************************//



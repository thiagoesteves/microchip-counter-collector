/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file config.h
 *
 * @author Thiago Esteves
 * @date 07/2008
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

//********************* Includes para o arquivo *****************************//

// Inclui o dispositivo
#include "16F873A.h"

// Inclui Typedefs
#include "typedefs.h"

//************************** Definições Internas ***************************//

// Definições gerais
#fuses XT, NOWDT , NOPUT, NOPROTECT, NOBROWNOUT, NOLVP
#use delay( clock = 4000000 )

// Confguração para a Porta Serial
#use rs232( baud = 9600, parity = N, xmit=PIN_C6, rcv=PIN_C7, bits = 8)

// Definição de timers
#define CONFIG_NUMBER_TIMER              6

#define CONFIG_MAIN_TIMER                0

// Sinaliza pinos fixos
#define CONFIG_LED_PIN                   PIN_C2
#define CONFIG_BUTTON_PIN                (input(PIN_C3))

//*************************** Protótipo de Funções **************************//

void init_device(void);

//***************************************************************************//

#endif //_CONFIG_H_

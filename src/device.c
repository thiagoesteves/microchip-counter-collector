/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file device.c
 *
 * @author Thiago Esteves
 * @date 10/2008
 */
   
//********************* Includes para o arquivo *****************************//

#include "config.h"
#include "device.h"
#include "timer.h"
#include "uart.h"
#include "lcd.h"

//********************* Constantes para o arquivo ***************************//

/**
 * Função que executa a tarefa de sincronização com o palm.
 *
 * @return TRUE/FALSE Retorna resultado da transferência
 */
uint8 device_task(void)
{
	uint8  tmp, i, addr;
	uint8  buff[29];
	uint16 size;
	
	// Verifica se chegaram 2 bytes
	if (uart_get_rx_cnt() == 2)
	{
		buff[0] = uart_read_byte();
		buff[1] = uart_read_byte();
	}
	else
	{
		buff[0] = uart_read_byte();
	}
	
	// Verifica se são válidos
	if ((buff[1] == 0x01) && (buff[0] == 10))
	{
		// Sincronizando
		printf(lcd_putc,"\f IR  CONECTADO ");
		
		// Verifica quantidade de bytes/relatórios gravados
		size = read_eeprom (101);
		size *= 10;
		
		for (i = 0; i < 3; i++)
			buff[i] = 0;
			
		// Caso n�o haja nenhum relatório
		if (size == 0)
		{
			uart_write_buffer(buff, 3);
			
			// Transação efetuada com sucesso
			printf(lcd_putc,"\nSUCESSO TRANSF.");
			delay_ms(1000);
			printf(lcd_putc,"\fINICIO RELATORIO");
			for (i = 0; i < 10; i++)
				uart_read_byte();
		}
		else
		{
			// Seleciona quantidade de bytes
			buff[2] = size;
			
			// Envia quantidade de bytes
			uart_write_buffer(buff, 3);
			
			delay_ms(300);
			
			addr = 0;
			// Envia os bytes ropriamente ditos
			while (size >= 29)
			{
				for (tmp = 0 ; tmp < 29; tmp++)
				{
					buff[tmp] = read_eeprom (addr + tmp);
				}
				size -= 29;
				addr += 29;
				
				// Envia bytes e aguarda resposta
				uart_write_buffer(buff, 29);
				delay_ms(200);
				while (uart_get_rx_cnt() == 0)
				{
					uart_write_buffer(buff, 29);
					delay_ms(200);
				}
				uart_read_byte();
			}
			if (size)
			{
				for (tmp = 0 ; tmp < size; tmp++)
				{
					buff[tmp] = read_eeprom (addr + tmp);
				}
				uart_write_buffer(buff, size);
				delay_ms(200);
				while (uart_get_rx_cnt() == 0)
				{
					uart_write_buffer(buff, size);
					delay_ms(200);
				}
				uart_read_byte();
			}
			
			// Aguarda Término
			while (uart_read_byte() != 0x02){}
			
			// Envia confirmação
			buff[0] = 2;
			uart_write_buffer(buff, 1);
			
			// Transação efetuada com sucesso
			printf(lcd_putc,"\nSUCESSO TRANSF.");
			delay_ms(1000);
			printf(lcd_putc,"\fINICIO RELATORIO");
			for (i = 0; i < 10; i++)
				uart_read_byte();
			
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
}


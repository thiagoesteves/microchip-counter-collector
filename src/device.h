/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file device.h
 *
 * @author Thiago Esteves
 * @date 10/2008
 */

#ifndef _DEVICE_H_
#define _DEVICE_H_

//******************* Protótipos de funções Externas ************************//

/**
 * Função que executa a tarefa de sincronização com o palm.
 *
 * @return TRUE/FALSE Retorna resultado da transferência
 */
uint8 device_task(void);

//***************************************************************************//

#endif // _DEVICE_H_






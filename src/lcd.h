/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file lcd.h
 *
 * @author Thiago Esteves
 * @date   10/2009
 */

#ifndef _LCD_H_
#define _LCD_H_


/**
 * Função de inicialização do módulo lcd
 */
void lcd_init(void);

/**
 * Função de envio de caracter para o módulo lcd
 */
void lcd_putc( char c);

#endif // _LCD_H_

/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */

/**
 *
 * @file main.c
 *
 * @author Thiago Esteves
 * @date 10/2009
 */ 

// Inclui  os módulos que irá utilizar
#include "config.h"
#include "timer.h"
#include "uart.h"
#include "lcd.h"
#include "device.h"

// Define um timer geral para o main
#define MAIN_TIMER  CONFIG_MAIN_TIMER

// Define Valor máximo de leitura analógica para sinalização
// de alinhamento (4,5 V) || 255 - 5.0v / X - 4,5v
#define MAIN_MIN_ALIGN              220

// Define Tamanho máximo de cada relatório
#define MAIN_MAX_REL                10
#define MAIN_SIZE_REL               10

// Mapeamento de memória EEPROM
// Sinaliza Endereços
#define MAIN_ADDR_KEY_EEPROM        100
#define MAIN_ADDR_REL_EEPROM        (MAIN_ADDR_KEY_EEPROM + 1)

// Sinaliza Valor default
#define MAIN_KEY_DATA_EEPROM        0xE5

struct main_data_
{
	uint8 key;        ///< Chave de validação
	uint8 rel_number; ///< Sinaliza quantos relatórios existem Atualmente
};

struct main_data_ main_data;

/**
 * Função que grava dados no relatório
 *
 * @param rel  
 * @param data 
 */
void main_save_data(uint8 rel, uint16 data);

//*********************************** Programa Principal *****************************//

void main(void)
{
	uint8 value, tmp;
	uint16 counter;
	
	init_device();                   // Inicializa as configurações do PIC.
	
	timer_init();                    // Inicializa Timers
	
	uart_init();                     // Inicializa UART
	
	lcd_init();                      // Inicializa LCD
	
	// Verifica validade dos dados
	main_data.key = read_eeprom(MAIN_ADDR_KEY_EEPROM);
	
	// Verifica se a chave é válida
	if (main_data.key != MAIN_KEY_DATA_EEPROM)
	{
		// Apaga memória inteira
		for (value = 0; value < 120; value++)
			write_eeprom(value, 0);
		
		// Insere chave de apagamento
		write_eeprom(MAIN_ADDR_KEY_EEPROM, MAIN_KEY_DATA_EEPROM);
	}
	
	// Carrega valores da eeprom
	main_data.rel_number = read_eeprom (MAIN_ADDR_REL_EEPROM);
	
	// Imprime mensagem inicial
	printf(lcd_putc,"TCC - BRUNO 2009");
	
	// delay de 1 segundo
	timer_set(MAIN_TIMER, 1000);
	while (timer_get(MAIN_TIMER, TIMER_EVENT) == 0){};
	
	// Mensagem esperando alinhamento
	printf(lcd_putc,"\f--ALINHAMENTO--");
	
	// Inicializa variável de leitura analógica
	value = 0xFF;
	
	// Verifica se o sistema está alinhado
	while ((value > MAIN_MIN_ALIGN) || (CONFIG_BUTTON_PIN))
	{
		// Efetua leitura Analógica
		value = read_adc();
		
		if (value > MAIN_MIN_ALIGN)
		{
			printf(lcd_putc,"\nSENSOR DESAL.   ");
			output_high (CONFIG_LED_PIN);
		}
		else
		{
			printf(lcd_putc,"\nSENSOR ALINHADO");
			output_low (CONFIG_LED_PIN);
		}
	}
	
	// Mensagem esperando alinhamento
	printf(lcd_putc,"\fINICIO RELATORIO");
	
	// Verifica se relatório está cheio
	if (main_data.rel_number >= MAIN_MAX_REL)
	{
		printf(lcd_putc,"\n MEMORIA  CHEIA ");
	}
	else
	{
		// Verifica se é um novo relatório
		while (main_data.rel_number < MAIN_MAX_REL)
		{
			// debouncig de tecla
			timer_set(MAIN_TIMER, 300);
			while (timer_get(MAIN_TIMER, TIMER_EVENT) == 0){};
			
			// Aguarda usuário soltar botão
			while (!CONFIG_BUTTON_PIN){};
			
			// Atualiza relatório
			main_data.rel_number++;
			
			// Inicializa contador
			counter = FALSE;
			
			// Inicializa flag
			tmp = TRUE;
			
			// Analisa quantidade de passagens
			while (CONFIG_BUTTON_PIN)
			{
				printf(lcd_putc,"\nREL %2u %4lu bois", main_data.rel_number, counter);
				
				// Efetua leitura Analógica
				value = read_adc();
				
				if ((value > MAIN_MIN_ALIGN) && (tmp == FALSE))
				{
					output_high (CONFIG_LED_PIN);
					counter++;
					
					// Atualiza contagem
					main_save_data(main_data.rel_number, counter);
					
					// debouncig de passagem do boi
					timer_set(MAIN_TIMER, 300);
					while (timer_get(MAIN_TIMER, TIMER_EVENT) == 0){};
					
					// Salva relatório na eeprom
					
					// Sinaliza que já passou por este if
					tmp = TRUE;
				}
				else if((value <= MAIN_MIN_ALIGN) && (tmp == TRUE))
				{
					output_low (CONFIG_LED_PIN);
					
					// Sinaliza que já passou por este if
					tmp = FALSE;
				}
				// Verrifica palm
				if (device_task())
				{
					// Apaga memória inteira
					for (value = 0; value < 120; value++)
						write_eeprom(value, 0);
					
					// Reinicializa microcontrolador
					reset_cpu();
				}
			}
		}
		if (main_data.rel_number >= MAIN_MAX_REL)
		{
			printf(lcd_putc,"\n MEMORIA  CHEIA ");
		}
	}
	while (TRUE)
	{
		if (device_task())
		{
			// Apaga memória inteira
			for (value = 0; value < 120; value++)
				write_eeprom(value, 0);
			
			// Reinicializa microcontrolador
					reset_cpu();
		}
	}
}


/**
 * Função que grava dados no relatório
 *
 * @param rel  
 * @param data 
 */
void main_save_data(uint8 rel, uint16 data)
{
	uint8 addr;
	uint16 tmp;
	
	// Posiciona ponteiro
	addr = ((rel - 1) * 10);
	
	// Insere palavra de alinhamento
	write_eeprom(addr++, 'R');
	write_eeprom(addr++, 'E');
	write_eeprom(addr++, 'L');
	
	// Insere Número do relatório
	if (rel == 10)
	{
		write_eeprom(addr++, '1');
		write_eeprom(addr++, '0');
	}
	else
	{
		write_eeprom(addr++, ' ');
		write_eeprom(addr++, (rel + 0x30));
	}
	
	write_eeprom(addr++, ' ');
	
	// Insere contagem de bois
	if (data < 9999)
	{
		if (data >= 1000)
		{
			tmp = (data % 1000);
			if (tmp == 0)
			{
				tmp = 0x31;
				write_eeprom(addr++, (uint8)tmp);
			}
			else
			{
				tmp += 0x30;
				write_eeprom(addr++, (uint8)tmp);
			}
			tmp -= 0x30;
			data -= (tmp * 1000);
		}
		else
		{
			write_eeprom(addr++, ' ');
		}
		
		if (data >= 100)
		{
			tmp = (data % 100);
			if (tmp == 0)
			{
				tmp = 0x31;
				write_eeprom(addr++, (uint8)tmp);
			}
			else
			{
				tmp += 0x30;
				write_eeprom(addr++, (uint8)tmp);
			}
			tmp -= 0x30;
			data -= (tmp * 100);
		}
		else
		{
			write_eeprom(addr++, ' ');
		}
		
		if (data >= 10)
		{
			tmp = (data % 10);
			if (tmp == 0)
			{
				tmp = 0x31;
				write_eeprom(addr++, (uint8)tmp);
			}
			else
			{
				tmp += 0x30;
				write_eeprom(addr++, (uint8)tmp);
			}
			tmp -= 0x30;
			data -= (tmp * 10);
		}
		else
		{
			write_eeprom(addr++, ' ');
		}
		
		write_eeprom(addr++, (data + 0x30));
	}
	else
	{
		write_eeprom(addr++, ' ');
		write_eeprom(addr++, 'E');
		write_eeprom(addr++, 'R');
		write_eeprom(addr++, 'R');
	}
	
	// Salva quantidade de relatórios até o momento
	write_eeprom(MAIN_ADDR_REL_EEPROM, rel);
}

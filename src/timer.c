/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file timer.c
 *
 * @author Thiago Esteves
 * @date 10/2008
 */
   
//********************* Includes para o arquivo *****************************//

#include "config.h"
#include "timer.h"

//******************* Protótipos de funções internas ************************//

struct timer_ 
{
	uint16 reload_value;             ///< Valor de Atualização
	uint16 atual_value;              ///< Valor atual de contagem
	uchar event_count;              ///< Quantidade de eventos ocorridos
};

struct timer_ timer[CONFIG_NUMBER_TIMER];
uchar counter;

/**
 * Funãço de inicialização do módulo timer, utilizando timer 2,
 * configura para estouro de 10ms em um clock de 16 Mhz
 */
void timer_init(void)
{
	uchar i;
	
	for (i = 0; i < CONFIG_NUMBER_TIMER; i++)
	{
		timer[i].reload_value = 0x00;
		timer[i].atual_value = 0x00;
		timer[i].event_count = 0x00;
	}
	// Interrupção de timer 2, estouro a cada 1 ms
	setup_timer_2 ( T2_DIV_BY_4, 0xF9, 1);
	enable_interrupts(INT_TIMER2);
}

/**
 * Cria, deleta ou atualiza um determinado timer
 *
 * @param indice_timer indice do timer a ser configurado
 * @param reload_value valor de estouro do timer, caso for zero desativa o timer
 * @retval 1 se houver sucesso
 * @retval 0 se houver erro
 */
uchar timer_set(uchar indice_timer, uint16 reload_value)
{
	// Analisa se o timer é válido
	if (indice_timer >= CONFIG_NUMBER_TIMER)
		return FALSE;
	
	// Verifica se é para ativar ou desativar o timer
	if (reload_value != 0)
	{
		timer[indice_timer].atual_value  = reload_value;
		timer[indice_timer].reload_value = reload_value;
		timer[indice_timer].event_count  = 0;
	}
	else
	{
		timer[indice_timer].atual_value  = 0;
		timer[indice_timer].reload_value = 0;
		timer[indice_timer].event_count  = 0;
		
	}
	
	return TRUE;
}

/**
 * Verifica se houve estouro do timer desejado. Se sim, decrementa o contador interno do timer,
 * indicando que a ação correspondente ao estouro deve ser executada.
 *
 * @param indice_timer indice para o timer que está sendo checado.
 * @param arg determina o retorno da função:
 *
 * TIMER_RELOAD_VALUE: função retorna período de overflow do timer
 *
 * TIMER_EVENT: função retorna TRUE caso houve estouro
 */
uchar timer_get(uchar indice_timer, uchar arg)
{
	if (arg == TIMER_RELOAD_VALUE)
	{
		return timer[indice_timer].reload_value;
	}
	
	else if (arg == TIMER_EVENT)
	{
		// Analisa se o timer é válido
		if (indice_timer >= CONFIG_NUMBER_TIMER)
			return FALSE;
		
		if (timer[indice_timer].event_count > 0)
		{
			timer[indice_timer].event_count--;
			return TRUE;
		}
		else
			return FALSE;
	}
}
 
/**
 * Função de tratamento das interrupção de timer2
 */

#INT_TIMER2

void timer2_isr(void)
{
	for (counter = 0; counter < CONFIG_NUMBER_TIMER; counter++)
	{
		if (timer[counter].reload_value != 0)
		{
			timer[counter].atual_value--;
			if (timer[counter].atual_value == 0)
			{
				timer[counter].event_count++;
				timer[counter].atual_value = timer[counter].reload_value;
			}
		}
	}
}

/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file timer.h
 *
 * @author Thiago Esteves
 * @date 10/2008
 */

#ifndef _TIMER_H_
#define _TIMER_H_

//**************************** Constantes *****************************//

#define TIMER_OFF             0x0000

// Constantes usadas na função timer_get
#define TIMER_RELOAD_VALUE    0
#define TIMER_EVENT           1

//******************* Protótipos de funções Externas ************************//


/**
 * Função de inicialização do módulo timer
 */
void timer_init(void);

/**
 * Cria, deleta ou atualiza um determinado timer dependendo dos parâmetros.
 *
 * @param indice_timer indice do timer que está sendo desativado.
 * @param reload_value valor de estouro do timer, caso for zero desativa o timer.
 * @retval 1 se houver sucesso
 * @retval 0 se houver erro
 */ 
uchar timer_set(uchar indice_timer, uint16 reload_value);

/**
 * Verifica se houve estouro do timer desejado. Se sim, decrementa o contador interno do timer,
 * indicando que a ação correspondente ao estouro deve ser executada.
 *
 * @param indice_timer indice para o timer que está sendo testado.
 * @param arg flag usada para determinar o retorno da função:
 *
 * TIMER_RELOAD_VALUE: função retorna período de overflow do timer
 *
 * TIMER_EVENT: função retorna TRUE caso houve estouro
 */
uchar  timer_get(uchar indice_timer, uchar arg);

//***************************************************************************//

#endif // _TIMER_H_






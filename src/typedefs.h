/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file typedefs.h
 *
 * @author Thiago Esteves
 * @date 07/2008
 */

#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

//********************* Definições de Tipos de Dados ********************//

typedef unsigned char  uchar;
typedef unsigned char  uint8;
typedef unsigned int16 uint16;
typedef unsigned long  uint32;

//*********************************************************************//

#endif

/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file uart.c
 *
 * @author Thiago Esteves
 * @date 10/2008
 */

/************************************* Inclusão de Bibliotecas ***********************************/

#include "config.h"
#include "uart.h"

/************************************** Definições de Constantes *********************************/

#ifndef CONFIG_NOT_COMPILE_UART_C  // Utilizado caso o usuário não queira compilar o módulo

#ifndef UART_RX_BUF_SIZE
	#define UART_RX_BUF_SIZE 5
#endif

#ifndef UART_TX_BUF_SIZE
	#define UART_TX_BUF_SIZE 30
#endif

// Constantes referentes ao status de funcionamento do driver
#define UART_LISTENING     0
#define UART_TRANSMITTING  1

/*********************************** Definições de Tipos de Dados ********************************/

struct buffer_
{
	uchar head;             ///< Sinaliza o início do buffer
	uchar tail;             ///< Sinaliza final do buffer
	uchar *buf;             ///< Ponteiro para o buffer
};

struct uart_
{
	uchar status;
	
	#if !defined(CONFIG_UART_TX_DISABLE)
		struct buffer_ tx;
	#endif
	
	#if !defined(CONFIG_UART_RX_DISABLE)
		struct buffer_ rx;
	#endif
};

/**************************************** Variáveis Globais **************************************/

#if !defined(CONFIG_UART_RX_DISABLE)
	uchar uart_rx_buf[UART_RX_BUF_SIZE];
#endif

#if !defined(CONFIG_UART_TX_DISABLE)
	uchar uart_tx_buf[UART_TX_BUF_SIZE];
#endif

struct uart_ uart;
uchar tmp;

/********************************************* Funções *******************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Funções de Transmissão                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
* Inicializa módulo uart
*/
void uart_init(void)
{
	#if !defined(CONFIG_UART_RX_DISABLE)
		uart.rx.buf = uart_rx_buf;
		uart.rx.head = 0;
		uart.rx.tail = 0;
	#endif

	#if !defined(CONFIG_UART_TX_DISABLE)
		uart.tx.buf = uart_tx_buf;
		uart.tx.head = 0;
		uart.tx.tail = 0;
	#endif
	
	uart.status = UART_LISTENING;
}

#if !defined(CONFIG_UART_TX_DISABLE)

/**
* Envia um único byte pela uart
*
* @param byte dado a ser enviado
*
* @retval UART_TX_OK envio realizado com sucesso
* @retval UART_TX_BUFFER_OVERFLOW nem todos os dados enviados com sucesso (buffer overflow)
*/
uchar uart_write_byte(uchar tmp)
{
	return uart_write_buffer(&tmp, 1);
}


/**
* Envia um buffer de dados pela uart
*
* @param *buf endereço do buffer contendo os dados a serem enviados
* @param size quantidade de bytes a serem enviados
*
* @retval UART_TX_OK envio realizado com sucesso
* @retval UART_TX_FAILURE nenhum dado enviado
* @retval UART_TX_BUFFER_OVERFLOW nem todos os dados enviados com sucesso (buffer overflow)
*/
uchar uart_write_buffer(uchar *buf, uchar size)
{
	uchar buffer_overflow;
	uchar trigger;
	uchar i;
	
	if (size == 0)
		return UART_TX_FAILURE;
	
	buffer_overflow = FALSE;
	
	// Analisa se máquina de transmissão já está em operação
	if (uart.status == UART_LISTENING)
		trigger = TRUE;
	else
		trigger = FALSE;
	
	for (i = 0; i < size; i++)
	{
		if ((uart.tx.head + 1) == uart.tx.tail)
			buffer_overflow = TRUE;
		
		// Essa condição impede o overflow do buffer de tx
		if (!buffer_overflow)
		{
			uart.tx.buf[uart.tx.head] = buf[i];
			
			if (uart.tx.head < (UART_TX_BUF_SIZE - 1))
				uart.tx.head++;
			
			else
				uart.tx.head = 0;
		}
		else
			break;
	}
	
	uart.status = UART_TRANSMITTING;
	
	// Ativa m�quina caso for necessário
	if (trigger)
		enable_interrupts ( INT_TBE );
		
	return buffer_overflow ? UART_TX_BUFFER_OVERFLOW : UART_TX_OK;
}

#endif




///////////////////////////////////////////////////////////////////////////////////////////////////
//                                        Funções de Recepção                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(CONFIG_UART_RX_DISABLE)

/**
* Verifica a quantidade de bytes recebidos pelo driver, ainda não lidos por nenhum cliente
*
* @return quantidade de bytes não lidos armazenados pelo driver
*/
uchar uart_get_rx_cnt(void)
{
	if (uart.rx.head == uart.rx.tail)
		return 0;
	
	if (uart.rx.head > uart.rx.tail)
		return (uart.rx.head - uart.rx.tail);
		
	return (uart.rx.head + (UART_RX_BUF_SIZE - uart.rx.tail));
}


/**
* Lê um único byte recebido pelo driver
* 
* @return dado lido
*/
uchar uart_read_byte(void)
{
	uchar i;
	
	i = uart.rx.tail;

	if (uart.rx.tail < UART_TX_BUF_SIZE - 1)
		uart.rx.tail++;
	else
		uart.rx.tail = 0;
	
	return (uart.rx.buf[i]);
}


/**
* Lê um buffer de dados recebidos pela uart
*
* @param *buf endereço do buffer que comportará os dados lidos
* @param size quantidade de bytes lidos com sucesso
*
* @return quantidade de bytes lidos
*/
uchar uart_read_buffer(uchar *buf, uchar size)
{
	uchar i;
	
	for (i = 0; i < size; i++)
	{
		if (uart.rx.tail != uart.rx.head)
		{
			buf[i] = uart.rx.buf[uart.rx.tail];

			if (uart.rx.tail < UART_TX_BUF_SIZE - 1)
				uart.rx.tail++;
			else
				uart.rx.tail = 0;
		}
		else
			break;
	}
	
	return i;
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Função de Interrupção                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CONFIG_UART_TX_DISABLE

	#INT_TBE
	
	/**
	* Tratamento de interrupçãoo da UART0 do microcontrolador de transmissão
	*/
	
	void isr_serial_tx(void)
	{
		// Analisa a existência de dados para enviar
		if (uart.tx.head == uart.tx.tail)
		{
			uart.status = UART_LISTENING;
			disable_interrupts ( INT_TBE );
		}
		
		else
		{
			putc(uart.tx.buf[uart.tx.tail]);
			
			tmp = uart.tx.tail + 1;
			if (tmp == UART_TX_BUF_SIZE)
				uart.tx.tail = 0;
			
			else
				uart.tx.tail++;
		}
	}
		
#endif

#ifndef CONFIG_UART_RX_DISABLE

	#INT_RDA
	
	/**
	* Tratamento de interrupção da UART0 do microcontrolador de recepção
	*/
	
	void isr_serial_rx(void)
	{
		// Essa condição impede o overflow do buffer de rx
		tmp = uart.rx.head;
		if (tmp >= uart.rx.tail || ((tmp + 1) < uart.rx.tail))
		{
			uart.rx.buf[tmp] = getc();
			
			tmp = uart.rx.head;
			if ((tmp + 1) == UART_RX_BUF_SIZE)
				uart.rx.head = 0;
			else
				uart.rx.head++;
		}
		enable_interrupts ( INT_RDA );
	}

#endif

#endif // CONFIG_NOT_COMPILE_UART_C

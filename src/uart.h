/*
 * Project: counter collector
 * Compiler: CCS PIC C Compiler
 * Version: 4.057
 */
 
 
/**
 *
 * @file uart.h
 *
 * @author Thiago Esteves
 * @date 10/2008
 */


#ifndef _UART_H_
#define _UART_H_

/************************************** Definição de Constantes **********************************/

// Constantes referentes ao retorno das funções uart_send_byte e uart_send_buffer
#define UART_TX_OK                1
#define UART_TX_FAILURE           2
#define UART_TX_BUFFER_OVERFLOW   3


// Constantes referentes à configuração de paridade em uart_init_module
#define UART_NO_PARITY     0
#define UART_ODD_PARITY    1
#define UART_EVEN_PARITY   2

/************************************** Protótipos de Funções ************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                        Funções de Recepção                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
* Inicializa módulo uart
*/
void uart_init(void);

/**
* Verifica a quantidade de bytes recebidos pelo driver, ainda não lidos por nenhum cliente
*/
uchar uart_get_rx_cnt(void);

/**
* Lê um único byte recebido pelo driver
* 
* @return dado lido
*/
uchar uart_read_byte(void);


/**
* Lê um buffer de dados recebidos pela uart
*
* @param *buf endereço do buffer que comportará os dados lidos
* @param size quantidade de bytes lidos com sucesso
*
* @return quantidade de bytes lidos
*/
uchar uart_read_buffer(uchar *buf, uchar size);


///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Funções de Transmissão                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
* Envia um único byte pela uart
*
* @param byte dado a ser enviado
*
* @retval UART_TX_OK envio realizado com sucesso
* @retval UART_TX_BUFFER_OVERFLOW nem todos os dados enviados com sucesso (buffer overflow)
*/
uchar uart_write_byte(uchar tmp);


/**
* Envia um buffer de dados pela uart
*
* @param *buf endereço do buffer contendo os dados a serem enviados
* @param size quantidade de bytes a serem enviados
*
* @retval TRUE envio realizado com sucesso
* @retval FALSE dados não enviados com sucesso
*/
uchar uart_write_buffer(uchar *buf, uchar size);

/*************************************************************************************************/

#endif //_UART_H_
